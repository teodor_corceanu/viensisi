drop table if exists Events;

create table Events
(
	event_id int(10) NOT NULL auto_increment,
	event_name varchar(50) NOT NULL,
	area_id int(10) NOT NULL references Areas(area_id),
	latitude double NOT NULL,
	longitude double NOT NULL,
	reported_by varchar(20) NOT NULL references Volunteers(username),
	date_time timestamp DEFAULT now(),
	description varchar(150) DEFAULT '',
	PRIMARY KEY(event_id)
)	
	ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

insert into Events(event_id, event_name, area_id,	latitude, longitude, reported_by, date_time, description)
	values (1, 'E-001', 1, 47.3437, 25.4035, 'user', now(), 'Deversare poluanti');