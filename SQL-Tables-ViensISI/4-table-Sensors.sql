drop table if exists Sensors;

create table Sensors
(
	sensor_id int(10) NOT NULL auto_increment,
	area_id int(10) NOT NULL references Areas(area_id),
	sensor_name varchar(50) DEFAULT '',
	latitude double NOT NULL,
	longitude double NOT NULL,
	PRIMARY KEY(sensor_id)
)	
	ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

insert into Sensors (sensor_id, area_id, sensor_name,	latitude, longitude) 
	values(1, 1, 'S-001', 47.3437, 25.4035);