drop table if exists Measurements;

create table Measurements
(
	measurement_id int(10) NOT NULL auto_increment,
	sensor_id int(10) NOT NULL references Sensors(sensor_id),
	date_time timestamp DEFAULT now(),
	opacity int(10) DEFAULT 0,
	ph double DEFAULT 0,
	temperature int(5) DEFAULT 0,
	fixed_residue int(10) DEFAULT 0,
	dissolved_oxygen int(10) DEFAULT 0,
	cyanides int(10) DEFAULT 0,
	chlorides int(10) DEFAULT 0,
	PRIMARY KEY(measurement_id)
)
	ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

insert into Measurements (measurement_id, sensor_id, date_time, opacity, ph, temperature, fixed_residue, dissolved_oxygen, cyanides, chlorides) 
	values(1, 1, now(), 5, 7.0, 11, 22, 33, 44, 55);