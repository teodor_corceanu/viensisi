drop table if exists Areas;

create table Areas
(
	area_id int(10) NOT NULL,
	area_name varchar(50) NOT NULL,
	PRIMARY KEY(area_id),
	UNIQUE(area_name)
)	
	ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
	
insert into Areas (area_id, area_name) 
	values(1, 'Vatra Dornei - Chiril');
	
insert into Areas (area_id, area_name) 
	values(2, 'Chiril - Lunca');
	
insert into Areas (area_id, area_name) 
	values(3, 'Lunca - Poiana Teiului');
	
insert into Areas (area_id, area_name) 
	values(4, 'Poiana Teiului - Bicaz');
	
insert into Areas (area_id, area_name) 
	values(5, 'Bicaz - Piatra Neamt');