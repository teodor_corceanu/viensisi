Pentru operatii cu baze de date:

- Eclipse ar trebui sa aiba Data Tools Platform SQL Development Tools (verifica
in Help->Installation Details->Installed Software). Daca nu este, trebuie 
instalat (Help->Install New Software...)

- Pentru conexiunea Java-SQL am descarcat MySQL Connector/J  de la adresa:
https://dev.mysql.com/downloads/file/?id=480292
si din arhiva default.mysql-connector-java-8.0.13.zip am extras fisierul
mysql-connector-java-8.0.13.jar, pe care l-am pus in folderul Eclipse si apoi
l-am adaugat in Project->Properties->Java Build Path->Libraries->Add External JARs.

- Pachetul Xampp este instalat (de la tema Colony) si contine serverul MySQL.
Pentru a accesa baza de date trebuie pornit serverul appache si baza de date
mysql din Xampp Control Panel.

- Pentru a crea baza de date si tabelele am folosit CLI cmd.exe in
folderul c:\xampp\mysql\bin (readme in folderul sql-tables-viensisi).
Se creaza astfel baza de date si tabelele folosind scripturile sql de acolo.
Dupa utilizare, acestea mai pot fi folosite la refacerea tabelelor initiale
(goale sau cu niste date initiale).

- Am scris clasa AccessDB in care am implementat conectarea la MySQL
si operatiile asupra BD (Select, Insert, Update, Delete). Metodele clasei 
pot fi folosite in aplicatie

- 