drop table if exists Volunteers;

create table Volunteers
(
	username varchar(20) NOT NULL,
	password varchar(20) NOT NULL,	
	email varchar(50) NOT NULL,	
	role varchar(20) DEFAULT 'volunteer',
	PRIMARY KEY(username),
	UNIQUE(email)
)	
	ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
	
insert into Volunteers (username, password, email, role) 
	values('admin', 'admin', 'dba@viensisi.ro', 'administrator');
insert into Volunteers (username, password, email, role) 
	values('user', 'user', 'usr-0@viensisi.ro', 'volunteer');