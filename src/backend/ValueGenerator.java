package backend;

import java.util.TimerTask;
import java.util.concurrent.ThreadLocalRandom;

public class ValueGenerator extends TimerTask {
	String dbTable = "viensisi.measurements";
	String dbFields = "sensor_id, opacity, ph, temperature, fixed_residue, dissolved_oxygen, cyanides, chlorides";
	String dbValues;
	Integer sensorCount;
	int currentSensor;
	
	public ValueGenerator() {
		dbValues = new String();
		sensorCount = 25;
	}

	public void run() {
		
		for(currentSensor = 1 ; currentSensor <= sensorCount ; currentSensor++) {
			int opa, ph_val, temp, resid, oxy, cyan, chlo;
			opa = ThreadLocalRandom.current().nextInt(0, 40 + 1);
			ph_val = ThreadLocalRandom.current().nextInt(0, 14 + 1);
			temp = ThreadLocalRandom.current().nextInt(0, 40 + 1);
			resid = ThreadLocalRandom.current().nextInt(0, 3000 + 1);
			oxy = ThreadLocalRandom.current().nextInt(1, 16 + 1);
			cyan = ThreadLocalRandom.current().nextInt(0, 100 + 1);
			chlo = ThreadLocalRandom.current().nextInt(0, 500 + 1);
			
			dbValues = "" + currentSensor + ", " + opa + ", " + ph_val + ", " + temp + ", " + resid + ", " + oxy + ", " + cyan + ", " + chlo;
			
			try {
				AccessDB.insert(dbTable, dbFields, dbValues);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
	}

}
