package backend;

import java.util.ArrayList;

import com.esri.core.geometry.Point;

public class Sensor {

	private Point location;
	private int area;
	private int id;
	private String name;
	
	public Sensor(Point location, int area, int id, String name) {
		this.location = location;
		this.area = area;
		this.id = id;
		this.name = name;
	}
	
	public Sensor() {
		super();
	}
	
	public Point getLocation() {
		return location;
	}
	
	public int getArea() {
		return area;
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setLocation(Point location) {
		this.location = location;
	}
	
	public void setArea(int area) {
		this.area = area;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public ArrayList<String> getLatestMeasurement() {
		String tableName = "viensisi.measurements t1"
				+ "inner join(select max(date_time) maxdate, sensor_id from measurements group by sensor_id) t2 "
				+ "on t1.sensor_id=t2.sensor_id"
				+ " and t1.date_time=t2.maxdate";
		String fields = "t2.maxdate, t1.opacity, t1.ph, t1.temperature, t1.fixed_residue,"
				+ " t1.dissolved_oxygen, t1.cyanides, t1.chlorides";
		String cond = "t1.sensor_id=" + this.id;
		ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
		
		try {
			result = AccessDB.select(tableName, fields, cond);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result.get(1);
	}
}
