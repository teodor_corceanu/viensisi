package backend;

public class Volunteer {

	private String username, email, password;
	
	public Volunteer(String username, String email, String password) {
		this.username = username;
		this.email = email;
		this.password = password;
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setEmail() {
		this.email = email;
	}
	
	@Override
	public String toString() {
		return this.username + " - " + this.email;
	}
}