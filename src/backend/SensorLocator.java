package backend;

import java.util.ArrayList;

import com.esri.core.geometry.Point;

/*
 * Class that helps locating sensors on 
 * an arbitrary map location
 */
public class SensorLocator {

	private Point location;
	private Sensor locatedSensor;
	
	private final double errorRange = 0.001;
	
	public SensorLocator(Point location) {
		this.location = location;
		locatedSensor = new Sensor();
	}
	
	public Sensor getLocatedSensor() {
		return locatedSensor;
	}
	
	public boolean locate() {
		
		Double latitude = location.getY();
		Double longitude = location.getX();
		
		/* DB */
		String tableName = "viensisi.sensors";
		String fields = "*";
		
		try {
			ArrayList<ArrayList<String>> result = 
					AccessDB.select(tableName, fields, "");
			
			for (ArrayList<String> sensor : result) {
				
				/* Skip table header */
				if (!(sensor.get(0).equals("sensor_id"))) {
					int sensorId = Integer.valueOf(sensor.get(0));
					int areaId = Integer.valueOf(sensor.get(1));
					String sensorName = sensor.get(2);
					double sensorLat = Double.valueOf(sensor.get(3));
					double sensorLong = Double.valueOf(sensor.get(4));
					
					if (Math.abs(latitude - sensorLat) <= errorRange && 
						Math.abs(longitude - sensorLong) <= errorRange) {
						locatedSensor.setId(sensorId);
						locatedSensor.setArea(areaId);
						locatedSensor.setName(sensorName);
						locatedSensor.setLocation(new Point(sensorLong, sensorLat));
						return true;
					}
				}
			}
			return false;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
