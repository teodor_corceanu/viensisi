package backend;

import java.time.LocalDate;
import java.util.concurrent.ThreadLocalRandom;

public class SetInitialMeasurements {
	
	public static void generateMeasurements(int number) {
		int i;
		String dbTable = "viensisi.measurements";
		String dbFields = "sensor_id, date_time, opacity, ph, temperature, fixed_residue, dissolved_oxygen, cyanides, chlorides";
		String dbValues = new String();
		Integer sensorCount = 25;
		int currentSensor;
		
		for(i = number ; i > 0 ; i--) {
			LocalDate dat = LocalDate.now().minusDays(i);
			for(currentSensor = 1 ; currentSensor <= sensorCount ; currentSensor++) {
				int opa, ph_val, temp, resid, oxy, cyan, chlo;
				opa = ThreadLocalRandom.current().nextInt(0, 40 + 1);
				ph_val = ThreadLocalRandom.current().nextInt(0, 14 + 1);
				temp = ThreadLocalRandom.current().nextInt(0, 40 + 1);
				resid = ThreadLocalRandom.current().nextInt(0, 3000 + 1);
				oxy = ThreadLocalRandom.current().nextInt(1, 16 + 1);
				cyan = ThreadLocalRandom.current().nextInt(0, 100 + 1);
				chlo = ThreadLocalRandom.current().nextInt(0, 500 + 1);
				
				dbValues = "" + currentSensor + ", '" + dat.toString() + "', " + opa + ", " + ph_val + ", " + temp + ", " + resid + ", " + oxy + ", " + cyan + ", " + chlo;
				
				try {
					AccessDB.insert(dbTable, dbFields, dbValues);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
