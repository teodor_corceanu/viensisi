package backend;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

public class CSVParser {

	private String filename;
	private ArrayList<HashMap<String, String>> data;
	private int numberOfCols, numberOfValues;
	
	public CSVParser(String filename, int numberOfCols, int numberOfValues) {
		this.filename = filename;
		this.numberOfCols = numberOfCols;
		this.numberOfValues = numberOfValues;
		data = new ArrayList<HashMap<String, String>>();
	}
	
	public ArrayList<HashMap<String, String>> parse() {
		
		String line;
		StringTokenizer tok;
		HashMap<String, String> dict;
		
		try {
			BufferedReader buf = new BufferedReader(new FileReader(filename));
			
			/* Read csv header */
			String[] header = new String[numberOfCols];
			line = buf.readLine();
			tok = new StringTokenizer(line, ",");
			
			for (int i = 0; i < header.length; i++) {
				header[i] = tok.nextToken();
			}
			
			for (int i = 0; i < numberOfValues; i++) {
				dict = new HashMap<String, String>();
				line = buf.readLine();					
				tok = new StringTokenizer(line, ",");
				for (int j = 0; j < header.length; j++) {
					dict.put(header[j], tok.nextToken());					
				}
				data.add(dict);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return data;
	}
}