package backend;

import java.sql.Date;

import com.esri.core.geometry.Point;

public class Event {

	private String name, description, reporter;
	private String date;
	private Point location;
	
	public Event(String name, String description, String reporter,
				String date, Point location) {
	
					this.name = name;
					this.description = description;
					this.reporter = reporter;
					this.date = date;
					this.location = location;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public String getReporter() {
		return reporter;
	}
	
	public String getDate() {
		return date;
	}
	
	public Point getLocation() {
		return location;
	}
	
	public String toString() {
		return name + " " + reporter;
	}
}