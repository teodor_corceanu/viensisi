package backend;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class AccessDB {

//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//
//	}
//	
	public static Connection getConnection(String src) throws Exception {
		try {
			String url = "jdbc:mysql://localhost";
			String username = "root";
			String password = "";
			Connection conn = DriverManager.getConnection(url, username, password);
			PreparedStatement stmnt = conn.prepareStatement("use viensisi");
			ResultSet rez = stmnt.executeQuery();
			stmnt = conn.prepareStatement("show tables");
			rez = stmnt.executeQuery();
			//System.out.println("Connection Ok " + src);
			return conn;
		} catch(Exception e) {
			System.out.println(e);
		}
		return null;
	}
	
	public static ArrayList<ArrayList<String>> select(String tableName, String fields, String cond ) throws Exception {
		try {
			Connection conn = getConnection("Select");
			String query = new String("SELECT " + fields + " FROM " + tableName);
			if(cond != "")
				query = query + " WHERE " + cond;
			query = query + ";";
			PreparedStatement stmnt = conn.prepareStatement(query);
			ResultSet rez = stmnt.executeQuery();
			ArrayList<String> rd = new ArrayList<String>();
			ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
			for(int i = 1; i <= rez.getMetaData().getColumnCount(); i++)
				rd.add(rez.getMetaData().getColumnLabel(i));
			result.add((ArrayList<String>)rd.clone());
			while(rez.next()) {
				rd.clear();
				for(int j = 1; j <= rez.getMetaData().getColumnCount(); j++) {
					rd.add(rez.getString(j));
					if(rez.wasNull())
						rd.set(j - 1, "NULL");
				}
				result.add((ArrayList<String>)rd.clone());
			}
			conn.close();
			return result;
		} catch(Exception e) {
			System.out.println(e);
		}
		return null;
	}
	
	public static int insert(String tableName, String fields, String values ) throws Exception {
		try {
			Connection conn = getConnection("Insert");
			String query = new String("INSERT INTO " + tableName +"(" + fields + ") VALUES (" + values + ");");
			PreparedStatement stmnt = conn.prepareStatement(query);
			stmnt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
			ResultSet rez = stmnt.getGeneratedKeys();
			int nr = 0;
			if(rez.next()){
				nr = rez.getInt(1);
			}
			conn.close();
			return nr;
		} catch(Exception e) {
			System.out.println(e);
			return -1;
		}
	}
	
	public static int update(String tableName, String field_val, String cond ) throws Exception {
		try {
			Connection conn = getConnection("Update");
			String query = new String("UPDATE " + tableName +" SET " + field_val);
			if(cond != "")
				query = query + " WHERE " + cond;
			query = query + ";";
			PreparedStatement stmnt = conn.prepareStatement(query);
			int rez = stmnt.executeUpdate(query);
			conn.close();
			return rez;
		} catch(Exception e) {
			System.out.println(e);
			return -1;
		}
	}
	
	public static int delete(String tableName, String cond ) throws Exception {
		try {
			Connection conn = getConnection("Delete");
			String query = new String("DELETE FROM " + tableName);
			if(cond != "")
				query = query + " WHERE " + cond;
			query = query + ";";
			PreparedStatement stmnt = conn.prepareStatement(query);
			int rez = stmnt.executeUpdate(query);
			conn.close();
			return rez;
		} catch(Exception e) {
			System.out.println(e);
			return -1;
		}
	}

}
