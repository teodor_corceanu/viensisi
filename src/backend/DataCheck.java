package backend;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.SpatialReference;
import com.esri.map.JMap;

public final class DataCheck {

	private DataCheck() {}
	
	private static int usernameMaxLength = 20;
	private static int passwordMaxLength = 20;
	private static int emailMaxLength = 50;
	
	/*
	 * This returns a hashmap with entries like
	 * 		{<FIELD>, <MESSAGE>}
	 */
	public static HashMap<String, String> validateSignupData
		(String username, String email, String password) {
		
		HashMap<String, String> result = new HashMap<String, String>();
		StringBuffer message;
	
		/* Check username */
		if (username.length() == 0) {
			message = new StringBuffer("Empty");
		}
		else {
			if (username.length() > usernameMaxLength) {
				message = new StringBuffer("Too long");
			}
			else {
				try {
					ArrayList<ArrayList<String>> selUsr = AccessDB.select(
							"viensisi.volunteers", "*", "username='" + username + "'");	
					if (selUsr.size() > 1) {
						message = new StringBuffer("Duplicate");
					}
					else {
						message = new StringBuffer("OK");
					}
					result.put(username, message.toString());
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		/* Check email */
		if (email.length() == 0) {
			message = new StringBuffer("Empty");
		}
		else {
			if (email.length() > emailMaxLength) {
				message = new StringBuffer("Too long");
			}
			else {
				/* Dumbest regex ever but hopefully it does not reject valid emails */
				Pattern emailFormat = Pattern.compile("[A-Za-z0-9.-_]+@[A-Za-z0-9.-_]+");
				Matcher matcher = emailFormat.matcher(email);
				if (!(matcher.find())) {
					message = new StringBuffer("Invalid");
				}
				else {
					try {
						ArrayList<ArrayList<String>> selEmail = AccessDB.select(
								"viensisi.volunteers", "*", "email='" + email + "'");
							if (selEmail.size() > 1) {
								message = new StringBuffer("Duplicate");
							}
							else {
								message = new StringBuffer("OK");
							}	
							result.put(email, message.toString());
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
			
		/* Check password */
		if (password.length() == 0) {
			message = new StringBuffer("Empty");
		}
		else {
			if (password.length() > passwordMaxLength) {
				message = new StringBuffer("Too long");
			}
			else {
				message = new StringBuffer("OK");
			}
		}
		result.put(password, message.toString());
		
		return result;
	}
	
	/*
	 * dest can be - 'database' or 'map'
	 */
	public static Vector<Point> addSensorsIntoSystem(String sensorsFile, String dest) {

		String dbTable = "viensisi.sensors";
		String dbFields = "sensor_id, area_id, sensor_name, latitude, longitude";
	
		ArrayList<Sensor> sensors = new ArrayList<Sensor>();
		/* Get sensors data */
		CSVParser csvParser = new CSVParser(sensorsFile, 5, 25);
		ArrayList<HashMap<String, String>> sensorData = csvParser.parse();
		
		Vector<Point> sensorLocations = new Vector<Point>();
		
		for (HashMap<String, String> sensor : sensorData) {
			
			String dbValues = sensor.get("id") + ", " +
							sensor.get("area") + ", \"" +
							sensor.get("name") + "\", " +
							sensor.get("lat") + ", " +
							sensor.get("long");
			if (dest.equals("database")) {
				try {
					AccessDB.insert(dbTable, dbFields, dbValues);
				}
				catch (Exception e) {
					e.printStackTrace();
				}				
			}
			else {
				if (dest.equals("map")) {
					
					double longitude = Double.valueOf(sensor.get("long"));
					double latitude = Double.valueOf(sensor.get("lat"));
					
					Point pointGeometry = new Point(longitude, latitude);
					sensorLocations.add(pointGeometry);
				}
			}
		}
		return sensorLocations;
	}
	
	public static Vector<Volunteer> getVolunteersFromDB() {
		
		String dbTable = "viensisi.volunteers";
		String dbFields = "username, password, email";
				
		Vector<Volunteer> volunteers = new Vector<Volunteer>();
		
		try {
			ArrayList<ArrayList<String>> result = 
					AccessDB.select(dbTable, dbFields, "");
			
			for (ArrayList<String> line : result) {
				
				/* Skip table header */
				if (!(line.get(0).equals("username"))) {
					String username = line.get(0);
					String email = line.get(2);
					String password = line.get(1);
					volunteers.add(new Volunteer(username, email, password));
				}
			}
			return volunteers;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Point getGPSCoordinates(JMap map, Point rawPoint) {
		
		SpatialReference GPSReference = SpatialReference.create(4326);
		
		return (Point) GeometryEngine.project(rawPoint,
				map.getSpatialReference(), GPSReference);
	}

	public static String[] getMeasurementsFields() {
		
		String dbTable = "viensisi.measurements";
		String dbFields = "*";
		String[] headers;
		
		try {
			ArrayList<ArrayList<String>> result = 
					AccessDB.select(dbTable, dbFields, "");	
			ArrayList<String> tableHeader = result.get(0);
			headers = new String[tableHeader.size() - 3];
			
			for (int i = 0; i < tableHeader.size() - 3; i++) {
				String field = tableHeader.get(i + 3);
				if (field.equals("opacity")) {
					headers[i] = "Turbiditate";
				}
				if (field.equals("ph")) {
					headers[i] = "PH";
				}
				if (field.equals("temperature")) {
					headers[i] = "Temperatura";
				}
				if (field.equals("fixed_residue")) {
					headers[i] = "Reziduu fix";
				}
				if (field.equals("dissolved_oxygen")) {
					headers[i] = "Oxigen dizolvat";
				}
				if (field.equals("cyanides")) {
					headers[i] = "Concentratie cianuri";
				}
				if (field.equals("chlorides")) {
					headers[i] = "Concentratie cloruri";
				}
			}
			return headers;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void insertEventIntoDB(String name, String description, String username, Point location) {
		
		Double latitude = location.getY();
		Double longitude = location.getX();
		
		int area_id = 0;	//We can't find this out yet so we make abstraction of it
		
		/* DB */
		String dbTable = "viensisi.events";
		String dbFields = "event_name, area_id, latitude, longitude, reported_by, description";
		String dbValues = "'" + name + "', " + area_id + ", " + 
						latitude + ", " + longitude + ", " + "'" + username + "', " +
						"'" + description + "'";
		
		try {
			AccessDB.insert(dbTable, dbFields, dbValues);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static Vector<Event> getEventsFromDB() {
		
		Vector<Event> events = new Vector<Event>();
		
		/* DB */
		String dbTable = "viensisi.events";
		String dbFields = "event_name, latitude, longitude, reported_by, date_time, description";	
		
		try {
			ArrayList<ArrayList<String>> result = 
					AccessDB.select(dbTable, dbFields, "");
			
			for (ArrayList<String> line : result) {
				
				if (!(line.get(0).equals("event_name"))) {
					String name = line.get(0);
					Double latitude = Double.valueOf(line.get(1));
					Double longitude = Double.valueOf(line.get(2));
					String volunteerName = line.get(3);
					String date = line.get(4);
					String description = line.get(5);
					
					Point location = new Point(longitude, latitude);
					
					Event e = new Event(name, description, volunteerName,
										date, location);
					events.add(e);
					
					
				}
			}
			return events;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
}