package map;

import java.awt.Color;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Vector;

import com.esri.core.geometry.Point;
import com.esri.core.symbol.SimpleMarkerSymbol;
import com.esri.core.symbol.SimpleMarkerSymbol.Style;
import com.esri.map.GraphicsLayer;
import com.esri.map.JMap;
import com.esri.map.MapEvent;
import com.esri.map.MapEventListenerAdapter;
import com.esri.map.MapOptions;
import com.esri.map.MapOptions.MapType;
import com.esri.map.MapTip;

import backend.DataCheck;
import backend.Event;
import main.Project;

public class RiverMap {

	private RiverMap() {}
	
	private static RiverMap instance = null;
	private static JMap map = null;
	
	private static double centerLat = 47.3437;
	private static double centerLong = 25.4035;
	private static int zoomLevel = 14;
	
	public static GraphicsLayer eventLayer;
	public static SimpleMarkerSymbol SENSOR_SYMBOL = new SimpleMarkerSymbol(Color.BLACK, 10, Style.SQUARE);
	public static SimpleMarkerSymbol EVENT_SYMBOL = new SimpleMarkerSymbol(Color.RED, 15, Style.CIRCLE);
	
	public static void addSensorLayer() {
		
		GraphicsLayer sensorLayer = new GraphicsLayer();
		map.getLayers().add(sensorLayer);
		
		/* Get sensor data */
		Vector<Point> sensorLocations = DataCheck.addSensorsIntoSystem(Project.sensorsFile, "map");
		
		/* For each sensor, add it to sensor layer */
		for (Point sensorLocation : sensorLocations) {
			sensorLayer = Project.addMapPoint(sensorLayer, SENSOR_SYMBOL, null, sensorLocation);
		}
	}
	
	public static void addEventLayer() {
		
		eventLayer = new GraphicsLayer();	
		map.getLayers().add(eventLayer);
		
		/* Search for events in database */
		Vector<Event> events = DataCheck.getEventsFromDB();
		
		if (!(events.isEmpty())) {
			
			for (Event e : events) {
				
				/* Firstly, configure attributes to be displayed in map tips */
				HashMap<String,Object> attributes = new HashMap<String, Object>();
				attributes.put("Locatie", "(" +
						String.format("%.2f", e.getLocation().getY()) + ", " +
						String.format("%.2f", e.getLocation().getX()) + ")");
				attributes.put("Eveniment", e.getName());
				attributes.put("Descriere", e.getDescription());
				attributes.put("Raportat la", e.getDate());
				attributes.put("Raportat de", e.getReporter());
				
				eventLayer = Project.addMapPoint(eventLayer, EVENT_SYMBOL, attributes, e.getLocation());
			}
		}
		
		/* Map tips for event layer */
	      LinkedHashMap<String, String> displayFields = new LinkedHashMap<String, String>();
	      displayFields.put("Locatie", "");
	      displayFields.put("Eveniment", "Eveniment: ");
	      displayFields.put("Descriere", "Descriere: ");
	      displayFields.put("Raportat de", "Raportat de: ");
	      // create the MapTip and set it on the feature layer
	      MapTip mapTip = new MapTip(displayFields);
	      eventLayer.setMapTip(mapTip);		
	}
	
	public static void renderMap() {
		MapOptions mapOptions = new MapOptions(MapType.TOPO, centerLat, centerLong, zoomLevel);
		map = new JMap(mapOptions);
		map.addMapEventListener(new MapEventListenerAdapter() {
			
			  @Override
			  public void mapReady(MapEvent event) {
			    addSensorLayer();
			    addEventLayer();
			  }
		});
			
	}
	
	public static RiverMap getInstance() {
		
		if (instance == null) {
			instance = new RiverMap();
			renderMap();
			return instance;
		}
		else {
			renderMap();
			return instance;
		}
	}
	
	public JMap getMap() {
		return map;
	}
}