package main;

import java.util.HashMap;

import javax.swing.JPanel;

import backend.Volunteer;
import frontend.MainPage;
import frontend.NotificationsPage;
import frontend.SettingsPage;

public class Pages {
	
	/*
	 * Ca sa nu creem inutil un obiect de tip pagina la fiecare apasare de buton care 
	 * schimba pagina, o sa tinem un dictionar de pagini.
	 * 
	 * Ne trebuie pentru aplicatie o singura pagina de login, o singura pagina de autentificare,
	 * o singura pagina de grafice, o pagina principala pentru vizitatori si 
	 * cate o pagina principala, una de setari, una de notificari pentru fiecare voluntar din sistem
	 * (inclusiv pt admin, adminul o sa fie un voluntar special cu credentialele admin/admin)
	 * 
	 * Dictionarul o sa aiba intrari de forma <Cheie: String, Valoare: JPanel> 
	 * string-urile vor fi gen:
	 * 		*** "login", "signup", "plots"
	 * 		*** "visitor-main"
	 * 		*** "<USER>-main", "<USER>-settings", "<USER>-notifications"
	 */
	
	private static HashMap<String, JPanel> pages;
	
	private static Pages instance = null;
	
	private Pages() {}
	
	public static Pages getInstance() {
		if (instance == null) {
			pages = new HashMap<String, JPanel>();
			instance = new Pages();
			return instance;
		}
		else {
			return instance;
		}
	}
	
	public void addPage(String identifier, JPanel page) {
		pages.put(identifier, page);
	}
	
	public JPanel getPage(String identifier) {
		return pages.get(identifier);
	}
	
	public void createPages(Volunteer v) {
		
		String username = v.getUsername();
		Pages.getInstance().addPage(username + "-main", new MainPage("volunteer", v));
		Pages.getInstance().addPage(username + "-settings", new SettingsPage(v));
		Pages.getInstance().addPage(username + "-notifications", new NotificationsPage(v));
	}
	
	@Override
	public String toString() {
		return pages.keySet().toString();
	}
}