package main;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.Timer;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JFrame;

import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.map.Graphic;
import com.esri.core.symbol.SimpleMarkerSymbol;
import com.esri.map.GraphicsLayer;

import backend.DataCheck;
import backend.SetInitialMeasurements;
import backend.ValueGenerator;
import backend.Volunteer;
import frontend.LoginPage;
import frontend.NotificationsPage;
import frontend.PlottingPage;
import frontend.SignupPage;
import map.RiverMap;

public class Project {

	private static Project instance = null;
	
	private JFrame window;
	private int sizeX = 1200;
	private int sizeY = 700;
	
	public static String sensorsFile = "sensors.csv";
	
	private Project() {};
	
	public static Project getInstance() {
		
		if (instance == null) {
			instance = new Project();
			/* Add pages that are unique */
			RiverMap.getInstance();
			Pages.getInstance().addPage("login", new LoginPage());
			Pages.getInstance().addPage("signup", new SignupPage());
			Pages.getInstance().addPage("plots", new PlottingPage());
			Pages.getInstance().addPage("notif", new NotificationsPage(new Volunteer("bla", "", "")));
			//Pages.getInstance().addPage("visitor-main", new MainPage("visitor"));
			
			return instance;
		}
		else {
			return instance;
		}
	}
	
	/* Create pages for volunteers that are already in the database */
	public void createVolunteersPages() {
		
		Vector<Volunteer> volunteers = DataCheck.getVolunteersFromDB();
		for (Volunteer v : volunteers) {
			Pages.getInstance().createPages(v);
		}
	}
	
	public static GraphicsLayer addMapPoint(GraphicsLayer layer, SimpleMarkerSymbol symbol,
			HashMap<String, Object> attributes, Point point) {
		
		SpatialReference GPSReference = SpatialReference.create(4326);
		
		Point place = (Point) GeometryEngine.project(point,
				GPSReference, layer.getSpatialReference());
		Graphic pointGraphic = new Graphic(place, symbol, attributes);
		layer.addGraphic(pointGraphic);
		return layer;
	}
	
	public void createGUI() {
		
		window = new JFrame();
		window.setPreferredSize(new Dimension(sizeX, sizeY));
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JComponent component = Pages.getInstance().getPage("login");
		component.setOpaque(true);
		window.setContentPane(component);
		window.pack();
		window.setVisible(true);
	}
	
	public void setComponent(JComponent component) {
		window.setContentPane(component);
		window.pack();
		window.setVisible(true);
	}
	
	public static void main(String[] args) {
		
		//DataCheck.addSensorsIntoSystem(sensorsFile, "database");
		SetInitialMeasurements.generateMeasurements(5);
		Timer timer = new Timer();
		timer.schedule(new ValueGenerator(), 0, 30000);	//30 sec
		Project.getInstance().createGUI();
	}
}