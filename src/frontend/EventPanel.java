package frontend;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EtchedBorder;

import com.esri.core.geometry.Point;
import com.esri.map.GraphicsLayer;

import backend.DataCheck;
import backend.Volunteer;
import main.Project;
import map.RiverMap;

/*
 * Panel that will appear next to the map when
 * a volunteer wishes to report an event
 */
public class EventPanel extends JPanel {

	private JLabel newEventLabel, displayLocationLabel,
				eventNameLabel, eventDescriptionLabel;
	
	private JTextField eventNameField;
	private JTextArea eventDescriptionField;
	private JCheckBox emailOption;
	private JButton reportButton;
	
	/* Location of the event */
	private Point location = new Point();
	
	/* Who reported it */
	private Volunteer volunteer;
	
	public EventPanel(Volunteer volunteer) {
		
		this.volunteer = volunteer;
		this.setLayout(new GridLayout(4, 1));
		this.setPreferredSize(new Dimension(400, 500));
		
		/* Title panel */
		JPanel titlePanel = new JPanel();
		titlePanel.setLayout(new BorderLayout());
		
		newEventLabel = new JLabel("Eveniment nou");
		newEventLabel.setFont(new Font("Arial", Font.BOLD, 18));
		
		displayLocationLabel = new JLabel("Locatie ");
		displayLocationLabel.setFont(new Font("Arial", Font.BOLD, 16));
		
		titlePanel.add(newEventLabel, BorderLayout.NORTH);
		titlePanel.add(displayLocationLabel, BorderLayout.CENTER);
		
		this.add(titlePanel);
		
		/* Body panel */
		JPanel bodyPanel = new JPanel();
		bodyPanel.setLayout(new GridLayout(3, 1));
		
		eventNameLabel = new JLabel("Adauga un titlu (max. 50 caractere)");
		eventNameLabel.setFont(new Font("Arial", Font.PLAIN, 12));
		
		eventNameField = new JTextField();
		
		eventDescriptionLabel = new JLabel("Descrie, pe scurt, evenimentul (max. 150 caractere)");
		eventDescriptionLabel.setFont(new Font("Arial", Font.PLAIN, 12));
		
		eventDescriptionField = new JTextArea();
		
		bodyPanel.add(eventNameLabel);
		bodyPanel.add(eventNameField);
		bodyPanel.add(eventDescriptionLabel);
		this.add(bodyPanel);
		
		/* Text area for description */
		JScrollPane js = new JScrollPane(eventDescriptionField);
		js.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		eventDescriptionField.setLineWrap(true);
		this.add(js);	
		
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new BorderLayout());
		
		/* Send email option */
		emailOption = new JCheckBox("Trimite notificarea si pe email");
		emailOption.setFont(new Font("Arial", Font.PLAIN, 11));
		bottomPanel.add(emailOption, BorderLayout.NORTH);
		
		/* Button */
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridBagLayout());
		
		reportButton = new JButton("Raporteaza");
		reportButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				/* Get text from GUI */
				String eventName = eventNameField.getText();
				String eventDescription = eventDescriptionField.getText();
				
				if (eventName.equals("")) {
					eventNameLabel.setForeground(Color.RED);
				}
				else {
					if (eventDescription.equals("")) {
						eventDescriptionLabel.setForeground(Color.RED);
					}
					else {
						eventNameLabel.setForeground(Color.BLACK);
						eventDescriptionLabel.setForeground(Color.BLACK);
			
						/* Insert the new event into database */
						DataCheck.insertEventIntoDB(eventName, eventDescription,
								volunteer.getUsername(), location);
						
						/* Update the map */
						GraphicsLayer eventLayer = RiverMap.eventLayer;
						HashMap<String,Object> attributes = new HashMap<String, Object>();
						attributes.put("Locatie", "(" +
								String.format("%.2f", location.getY()) + ", " +
								String.format("%.2f", location.getX()) + ")");
						attributes.put("Eveniment", eventName);
						attributes.put("Descriere", eventDescription);
						attributes.put("Raportat de", volunteer.getUsername());
						eventLayer = Project.addMapPoint(eventLayer, RiverMap.EVENT_SYMBOL, attributes, location);
						
						/* Clear text fields */
						eventNameField.setText("");
						eventDescriptionField.setText("");
						
						/* Hide itself and display logo label */
						EventPanel.this.setVisible(false);
						MainPage.showLogoLabel();
					}
				}
			}
		});
		
		buttonPanel.add(reportButton);
		
		bottomPanel.add(buttonPanel, BorderLayout.SOUTH);
		
		this.add(bottomPanel);
		this.setBorder(new EtchedBorder());
	}
	
	public void setOccurenceLocation(Point location) {
		this.location = location;
		displayLocationLabel.setText(
				"Locatie (" +
				String.format("%.2f", location.getY()) + ", " +
				String.format("%.2f", location.getX()) + ")");
	}
}
