package frontend;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import backend.AccessDB;
import backend.Volunteer;
import main.Pages;
import main.Project;
import map.RiverMap;

public class LoginPage extends JPanel {
	
	private JLabel userLabel, passLabel, logoLabel;
	private JLabel errorLabel;
	private JTextField userText;
	private JPasswordField passText;
	private JButton loginBtn, visitorBtn, signupBtn;
	private Font font, errorFont;
	
	private void resetInput() {
		
		userText.setText("");
		passText.setText("");
	}
	
	public LoginPage() {
		
		this.setLayout(new GridBagLayout());
		this.setBackground(Colors.BRIGHT_YELLOW);
		font = new Font("Arial", Font.PLAIN, 18);
		
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		
		/* Logo */
		logoLabel = new JLabel();
		logoLabel.setIcon(new ImageIcon("logo.png"));
		logoLabel.setBorder(new EmptyBorder(20, 20, 20, 20));
		
		c.ipady = 30;
		add(logoLabel);
		
		/* Panel with login data */
		JPanel loginPanel = new JPanel();
		loginPanel.setLayout(new GridLayout(2, 2));
		loginPanel.setBackground(Colors.BRIGHT_YELLOW);
		
		userLabel = new JLabel("Nume utilizator ");
		passLabel = new JLabel("Parola");
		userLabel.setFont(font);
		passLabel.setFont(font);
		userLabel.setForeground(Color.BLACK);
		passLabel.setForeground(Color.BLACK);
		userText = new JTextField();
		passText = new JPasswordField();
		loginPanel.add(userLabel);
		loginPanel.add(userText);
		loginPanel.add(passLabel);
		loginPanel.add(passText);
		loginPanel.setBorder(new EtchedBorder());
		
		c.gridy = 1;
		c.ipady = 0;
		add(loginPanel, c);
		
		/* JLabel with possible error message */
		errorLabel = new JLabel("Date de acces incorecte!");		
		errorLabel.setFont(errorFont);	
		errorLabel.setForeground(Color.RED);
		errorLabel.setVisible(false);
		
		c.gridy = 3;
		add(errorLabel, c);
		
		/* Buttons */
		JPanel btnPanel = new JPanel();
		btnPanel.setLayout(new GridLayout(3, 1));
		
		/* Login button */
		loginBtn = new JButton("Login");
		loginBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
		
				errorLabel.setVisible(false);
				/* Validate the login credentials using the database */
				String enterUser = userText.getText();
				String enterPass = String.valueOf(passText.getPassword());	
				
				ArrayList<ArrayList<String>> selEmail;
				try {
					selEmail = AccessDB.select(
						"viensisi.volunteers", "email",
						"(username='" + enterUser + "') AND (password='" + enterPass + "')");
					
					if (selEmail.size() < 2) {
						errorLabel.setVisible(true);
						/* Go to login again */
					}
					else {
						String enterMail = selEmail.get(1).get(0).toString();
						Volunteer volunteer = new Volunteer(enterUser, enterMail, enterPass);
						//JPanel mainPage = Pages.getInstance().getPage(enterUser + "-main");
						JPanel mainPage = new MainPage("volunteer", volunteer);
						Project.getInstance().setComponent(mainPage);
						resetInput();
					}	
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
			}
		});
		btnPanel.add(loginBtn);
		
		/* Sign up button will take us to the sign up page */
		signupBtn = new JButton("Creare cont");
		signupBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				errorLabel.setVisible(false);
				resetInput();
				JPanel signupPage = Pages.getInstance().getPage("signup");
				try {
					Project.getInstance().setComponent(signupPage);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnPanel.add(signupBtn);
		
		/* Visitor button will display the main page without many privileges */
		visitorBtn = new JButton("Intrare vizitator");
		visitorBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				errorLabel.setVisible(false);
				resetInput();
				//JPanel visitorPage = Pages.getInstance().getPage("visitor-main");
				JPanel visitorPage = new MainPage("visitor");
				try {
					Project.getInstance().setComponent(visitorPage);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnPanel.add(visitorBtn);
		btnPanel.setBackground(Colors.BRIGHT_YELLOW);
		btnPanel.setBorder(new EmptyBorder(20, 20, 20, 20));
		
		c.gridy = 2;
		add(btnPanel, c);
	}
}