package frontend;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import backend.AccessDB;
import backend.DataCheck;
import backend.Volunteer;
import main.Pages;
import main.Project;

public class SignupPage extends JPanel {

	private Font font, errorFont;
	private JLabel logoLabel, userLabel, emailLabel, passLabel, repeatPassLabel;
	private JLabel errorLabel;
	private JTextField userText, emailText;
	private JPasswordField passField, repeatPassField;
	private JButton signupBtn, clearBtn, backBtn;
	
	/* Read data inserted by the user */
	private String[] fetchSignupData() {
		
		String[] signupData = new String[3];
		signupData[0] = userText.getText();
		signupData[1] = emailText.getText();
		signupData[2] = String.valueOf(passField.getPassword());		
		
		return signupData;
	}
	
	/* Describe the behavior of the page when validating user data */
	
	private boolean validateSignupData() {

		String username = userText.getText();
		String email = emailText.getText();
		String password = String.valueOf(passField.getPassword());
		
		boolean filledFields = true;
		boolean checkData = false;
		
		if (username.equals("")) {
			userLabel.setForeground(Color.RED);
			filledFields = false;
		}
		
		if (email.equals("")) {
			emailLabel.setForeground(Color.RED);
			filledFields = false;
		}
		
		if (password.equals("")) {
			passLabel.setForeground(Color.RED);
			filledFields = false;
		}
		
		if (filledFields) {	
		
			checkData = true;
			
			/* Send data to backend in order to validate it */
			HashMap<String, String> dataStatus = DataCheck.validateSignupData(username, email, password);
			
			/* Validate username field */
			String usernameStatus = dataStatus.get(username);
			if (!(usernameStatus.equals("OK"))) {
				checkData = false;
				userLabel.setForeground(Color.RED);
				if (usernameStatus.equals("Duplicate")) {
					errorLabel.setText("User deja existent!");
					errorLabel.setVisible(true);							
				}
			}
			
			/* Validate email field */
			String emailStatus = dataStatus.get(email);
			if (!(emailStatus.equals("OK"))) {
				checkData = false;
				emailLabel.setForeground(Color.RED);
				if (emailStatus.equals("Duplicate")) {
					if(errorLabel.getText().length() == 0) {
						errorLabel.setText("Email deja folosit!");
					}
					errorLabel.setVisible(true);							
				}
			}
			
			/* Validate password field */
			String passwordStatus = dataStatus.get(password);
			if (!(passwordStatus.equals("OK"))) {
				checkData = false;
				passLabel.setForeground(Color.RED);
			}			
		}
		return checkData;
	}
	
	/* 
	 * Clears the text fields
	 * Sets the initial color for labels
	 */
	private void resetInput() {
		
		userText.setText("");
		emailText.setText("");
		passField.setText("");
		repeatPassField.setText("");
		
		userLabel.setForeground(Color.BLACK);
		emailLabel.setForeground(Color.BLACK);
		passLabel.setForeground(Color.BLACK);
		repeatPassLabel.setForeground(Color.BLACK);
	}

	/* Class constructor */
	public SignupPage() {
		
		this.setLayout(new GridBagLayout());
		this.setBackground(Colors.BRIGHT_YELLOW);
		font = new Font("Arial", Font.PLAIN, 18);
		errorFont = new Font("Arial", Font.BOLD, 14);

		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		
		/* Logo */
		logoLabel = new JLabel();
		logoLabel.setIcon(new ImageIcon("logo.png"));
		logoLabel.setBorder(new EmptyBorder(20, 20, 20, 20));
		
		c.ipady = 40;
		add(logoLabel, c);
		
		/* Panel with signup data */
		JPanel signupPanel = new JPanel();
		signupPanel.setLayout(new GridLayout(4, 4));
		signupPanel.setBackground(Colors.BRIGHT_YELLOW);
		
		userLabel = new JLabel("Nume utilizator ");
		emailLabel = new JLabel("Adresa e-mail");
		passLabel = new JLabel("Parola");
		repeatPassLabel = new JLabel("Repeta parola");
		userLabel.setFont(font);
		emailLabel.setFont(font);
		passLabel.setFont(font);
		repeatPassLabel.setFont(font);
		userLabel.setForeground(Color.BLACK);
		emailLabel.setForeground(Color.BLACK);
		passLabel.setForeground(Color.BLACK);
		repeatPassLabel.setForeground(Color.BLACK);
		
		userText = new JTextField();
		emailText = new JTextField();
		passField = new JPasswordField();
		repeatPassField = new JPasswordField();
		
		signupPanel.add(userLabel);
		signupPanel.add(userText);
		signupPanel.add(emailLabel);
		signupPanel.add(emailText);	
		signupPanel.add(passLabel);
		signupPanel.add(passField);
		signupPanel.add(repeatPassLabel);
		signupPanel.add(repeatPassField);
		signupPanel.setBorder(new EtchedBorder());
		
		c.gridy = 1;
		c.ipady = 0;
		add(signupPanel, c);
		
		/* JLabel with possible error message 
		 * 
		 * "User deja existent!"
		 * "Email deja existent!"
		 * "Parolele nu corespund!"
		 * 
		 * For any other errors (e.g. blank field), the label turns RED
		 */
		errorLabel = new JLabel();
		errorLabel.setFont(errorFont);
		errorLabel.setForeground(Color.RED);
		errorLabel.setText("");
		errorLabel.setVisible(false);
		
		c.gridy = 3;
		add(errorLabel, c);
		
		/* Panel with buttons */
		JPanel btnPanel = new JPanel();
		btnPanel.setLayout(new GridLayout(3, 1));
		
		/* Sign up button */
		signupBtn = new JButton("Creare cont");
		signupBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				/* Firstly, the two password fields must be identical */
				String pass1 = String.valueOf(passField.getPassword());
				String pass2 = String.valueOf(repeatPassField.getPassword());	
				if (!(pass1.equals(pass2))) {
					errorLabel.setText("Parolele nu corespund!");
					errorLabel.setVisible(true);
					passLabel.setForeground(Color.RED);
					repeatPassLabel.setForeground(Color.RED);
				}
				else {
					/* We can do further checks with the user provided data */
					userLabel.setForeground(Color.BLACK);
					emailLabel.setForeground(Color.BLACK);
					passLabel.setForeground(Color.BLACK);
					repeatPassLabel.setForeground(Color.BLACK);
					errorLabel.setVisible(false);	
					
					String[] data = SignupPage.this.fetchSignupData();	
	
					/* If data is valid, add volunter into the system */
					if (SignupPage.this.validateSignupData()) {
						SignupPage.this.resetInput();
						Volunteer newVolunteer = new Volunteer(data[0], data[1], data[2]);
						
						/* Add volunter into database! */
						try {
							int ins = AccessDB.insert("viensisi.volunteers", 
								"username, password, email, role",
								"'" + data[0] + "', '" + data[2] + "', '" + data[1] + "', 'volunteer'");
						}
						catch (Exception f) {
							f.printStackTrace();
						}
						
						/* Add pages for volunteer */
//						Pages.getInstance().addPage(data[0] + "-main", new MainPage("volunteer", newVolunteer));
//						Pages.getInstance().addPage(data[0] + "-settings", new SettingsPage(newVolunteer));
//						Pages.getInstance().addPage(data[0] + "-notifications", new NotificationsPage(newVolunteer));
						
						/* Go to login page */
						JPanel loginPage = Pages.getInstance().getPage("login");
						Project.getInstance().setComponent(loginPage);
					}
				}
			}
		});
		
		/* Clear all fields button */
		clearBtn = new JButton("Sterge");
		clearBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
			
				SignupPage.this.resetInput();
				errorLabel.setVisible(false);
			}
		});
		
		/* Back to login page button */
		backBtn = new JButton("Inapoi");
		backBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				SignupPage.this.resetInput();
				errorLabel.setVisible(false);
				JPanel loginPage = Pages.getInstance().getPage("login");
				try {
					Project.getInstance().setComponent(loginPage);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		btnPanel.add(signupBtn);
		btnPanel.add(clearBtn);
		btnPanel.add(backBtn);
		btnPanel.setBackground(Colors.BRIGHT_YELLOW);
		btnPanel.setBorder(new EmptyBorder(30, 30, 30, 30));
		
		c.gridy = 2;
		add(btnPanel, c);
	}
}