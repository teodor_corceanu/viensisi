package frontend;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ListIterator;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.ListCellRenderer;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import backend.DataCheck;
import backend.Event;
import backend.Volunteer;

public class NotificationsPage extends JPanel {

	private Volunteer user;
	private DefaultListModel<Event> listModel;
	private JList<Event> eventList;
	
	public static JPanel eventDetailsPanel;
	public static JLabel imageLabel;
	public static int previousIndex = 0;
	public static Vector<NotificationsPanel> notificationPanels;
	
	public static void setEventOnView(int index) {
		
		imageLabel.setVisible(false);
		notificationPanels.get(previousIndex).setVisible(false);
		
		eventDetailsPanel.removeAll();
		
		NotificationsPanel current = notificationPanels.get(index);
		current.setVisible(true);
		eventDetailsPanel.add(current);
		
		previousIndex = index;
	}
	
	public NotificationsPage(Volunteer user) {
		
		this.user = user;
		this.setLayout(new BorderLayout());
		this.setBackground(Colors.BRIGHT_YELLOW);
		
		notificationPanels = new Vector<NotificationsPanel>();
		
		/* Title panel */
		JPanel titlePanel = new JPanel();
		titlePanel.setLayout(new GridBagLayout());
		titlePanel.setBackground(Colors.DARK_BLUE);
		
		JLabel title = new JLabel("NOTIFICARI DE EVENIMENTE");
		title.setForeground(Color.WHITE);
		title.setFont(new Font("Verdana", Font.PLAIN, 30));
		
		titlePanel.add(title);
		
		this.add(titlePanel, BorderLayout.NORTH);
		
		/* Body panel */
		JPanel bodyPanel = new JPanel();
		bodyPanel.setLayout(new BorderLayout());
		bodyPanel.setBackground(Colors.BRIGHT_YELLOW);
		
		/* JList of events */
		listModel = new DefaultListModel<Event>();
		ListIterator<Event> itr = DataCheck.getEventsFromDB().listIterator();
		while (itr.hasNext()) {
			Event e = (Event)itr.next();
			listModel.addElement(e);
			notificationPanels.add(new NotificationsPanel(e));
		}
		eventList = new JList<Event>(listModel);
		eventList.setCellRenderer(new EventRenderer());
		
		/* Scrollpane with list */
		JScrollPane js = new JScrollPane(eventList);
		js.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		bodyPanel.add(js, BorderLayout.WEST);
		bodyPanel.setBorder(new EmptyBorder(35, 35, 35, 35));
		
		/* Event details panel */
		eventDetailsPanel = new JPanel();
		eventDetailsPanel.setBorder(new EtchedBorder());
		eventDetailsPanel.setPreferredSize(new Dimension(400, 300));
		
		imageLabel = new JLabel(new ImageIcon("logo-black.png"));
		eventDetailsPanel.add(imageLabel);
		bodyPanel.add(eventDetailsPanel, BorderLayout.EAST);
		
		this.add(bodyPanel, BorderLayout.CENTER);
		
		
		/* Back button panel */
		JPanel backButtonPanel = new JPanel();
		backButtonPanel.setLayout(new GridBagLayout());
		backButtonPanel.setBackground(Colors.BRIGHT_YELLOW);
		backButtonPanel.setBorder(new EmptyBorder(60, 100, 20, 100));
		
		JButton backBtn = new JButton("Inapoi");
		backBtn.addActionListener(new ActionListener() {;
		
			@Override
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		
		backButtonPanel.add(backBtn);
		
		this.add(backButtonPanel, BorderLayout.SOUTH);
	}
	
	public Volunteer getUser() {
		return user;
	}
}

class NotificationsPanel extends JPanel {
	
	private Event event;
	
	public NotificationsPanel(Event event) {
		this.event = event;
		
		this.setLayout(new BorderLayout());
		
		
		/* Title panel */
		JPanel titlePanel = new JPanel();
		titlePanel.setLayout(new BorderLayout());
		
		JLabel title = new JLabel(event.getName());
		title.setFont(new Font("Verdana", Font.BOLD, 24));
		
		JLabel dateLabel = new JLabel("Raport eveniment");
		dateLabel.setFont(new Font("Verdana", Font.ITALIC, 14));
		
		titlePanel.add(title, BorderLayout.NORTH);
		titlePanel.add(dateLabel, BorderLayout.SOUTH);
		
		this.add(titlePanel, BorderLayout.NORTH);
		
		
		/* Body panel */
		JPanel bodyPanel = new JPanel();
		bodyPanel.setLayout(new BorderLayout());
		
		
		/* Description panel */
		JPanel descriptionPanel = new JPanel();
		descriptionPanel.setPreferredSize(new Dimension(300, 300));
		descriptionPanel.setLayout(new GridLayout(1, 1));
		descriptionPanel.setBorder(new EmptyBorder(20, 20, 20, 20));
		JLabel descriptionLabel = new JLabel();
		descriptionLabel.setText("<html><p>" + event.getDescription() + "</p></html>");
		descriptionLabel.setFont(new Font("Verdana", Font.PLAIN, 16));
		descriptionPanel.add(descriptionLabel);
		
		bodyPanel.add(descriptionPanel, BorderLayout.NORTH);

		bodyPanel.add(new JSeparator(JSeparator.HORIZONTAL));
		this.add(bodyPanel, BorderLayout.CENTER);
		
		
		/* Bottom panel */
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new GridLayout(1, 2));
		
		/* Reporter panel */
		JPanel reporterPanel = new JPanel();
		reporterPanel.setLayout(new GridLayout(2, 1));
		
		/* Fill the reporter panel */
		JLabel reportedBy = new JLabel("Raportat de");
		reportedBy.setFont(new Font("Verdana", Font.PLAIN, 14));
		
		JLabel reporter = new JLabel(event.getReporter());
		reporter.setFont(new Font("Verdana", Font.BOLD, 18));
		
		reporterPanel.setBorder(new EmptyBorder(20, 20, 20, 20));
		reporterPanel.add(reportedBy);
		reporterPanel.add(reporter);
		
		/* View map button panel */
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridBagLayout());
		
		JButton viewMapButton = new JButton("Vezi pe harta");
		viewMapButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Show main page by zooming on the event
			}
		});
		
		buttonPanel.add(viewMapButton);
		
		bottomPanel.add(reporterPanel);
		bottomPanel.add(buttonPanel);
		
		this.add(bottomPanel, BorderLayout.SOUTH);
		this.setVisible(true);
	}

}

class EventRenderer extends JPanel implements ListCellRenderer<Event> {
	
	private JLabel title, date, reporter;
	//private Label img;
	
	public EventRenderer() {
		title = new JLabel();
		date = new JLabel();
		//img = new JLabel();
	}
	
	@Override
	public Component getListCellRendererComponent(
			JList<? extends Event> list, Event event, int index,
			boolean isSelected, boolean hasFocus) {
		
		/* Set data */
		title.setText(event.getName());
		date.setText(event.getDate());
	
		title.setFont(new Font("Verdana", Font.BOLD, 22));
		date.setFont(new Font("Verdana", Font.ITALIC, 14));
		
		setLayout(new GridLayout(2, 1));
		if (isSelected) {
			this.setBackground(Colors.BRIGHT_GREEN);
			/* View details for the new selected event */
			NotificationsPage.setEventOnView(index);
		}
		else {
			this.setBackground(Color.WHITE);
		}
		
		super.add(title);
		super.add(date);
		
		return this;
	}
}