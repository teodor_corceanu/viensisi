package frontend;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import main.Pages;
import main.Project;

/*
 * This class describes the header of the main page
 */
public class HeaderPanel extends JPanel {
	
	private String username;
	/*
	 * When clicking on a button, the user navigates to the specified 
	 * component
	 */
	private void defineListener(JButton button, JComponent component) {
		
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Project.getInstance().setComponent(component);
			}
		});
	}

	public HeaderPanel(String username) {
		
		this.username = username;
		this.setLayout(new BorderLayout());
		this.setBackground(Colors.DARK_BLUE);
		
		/* JPanel that displays username */
		JPanel usernameDisplayPanel = new JPanel();
		usernameDisplayPanel.setLayout(new GridLayout(1, 2));
		usernameDisplayPanel.setBackground(Colors.DARK_BLUE);
		
		JLabel loggedInAsLabel = new JLabel("Logat ca ");
		loggedInAsLabel.setForeground(Color.WHITE);
		loggedInAsLabel.setFont(new Font("Verdana", Font.PLAIN, 18));
		
		JLabel userLabel = new JLabel(this.username);
		userLabel.setForeground(Color.WHITE);
		userLabel.setFont(new Font("Verdana", Font.BOLD, 18));
		
		usernameDisplayPanel.add(loggedInAsLabel);
		usernameDisplayPanel.add(userLabel);
		
		/* Add username display panel to header panel */
		this.add(usernameDisplayPanel, BorderLayout.WEST);
		
		/* JPanel with buttons */
		JPanel buttonsPanel = new JPanel();
	
		if (this.username.equals("visitor")) {

			buttonsPanel.setLayout(new GridLayout(1, 1));
			
			JButton exitBtn = new JButton("Exit");
			
			this.defineListener(exitBtn, Pages.getInstance().getPage("login"));
			buttonsPanel.add(exitBtn);
		}
		else {
			buttonsPanel.setLayout(new GridLayout(1, 4));
			
			JButton notifBtn = new JButton("Notificari");
			JButton setBtn = new JButton("Setari");
			JButton plotBtn = new JButton("Grafice");
			JButton outBtn = new JButton("Logout");
			
			//this.defineListener(notifBtn, Pages.getInstance().getPage(this.username + "-notifications"));
			this.defineListener(setBtn, Pages.getInstance().getPage(this.username + "-settings"));
			this.defineListener(plotBtn, Pages.getInstance().getPage("plots"));
			this.defineListener(outBtn, Pages.getInstance().getPage("login"));
			
			buttonsPanel.add(notifBtn);
			buttonsPanel.add(setBtn);
			buttonsPanel.add(plotBtn);	
			buttonsPanel.add(outBtn);
		}
		
		/* Add buttons panel to header panel */
		this.add(buttonsPanel, BorderLayout.EAST);
	}
}
