package frontend;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EtchedBorder;

import backend.DataCheck;

/*
 * Panel that will appear next to the map 
 * in the main page when the user clicks
 * on a sensor
 * 
 * It contains that sensor's details (measurements)
 */
public class SensorPanel extends JPanel {

	/* Labels with current selected sensor */
	private JLabel currentSensorTitle, currentSensorArea;
	private JLabel latestSensorMeasurement;
	
	/* Table with latest measurements */
	private JTable measurementsTable;
	
	/*
	 * Create JTable in which latest measurements of a sensor will be displayed
	 */
	private JTable createMeasurementsTable() {
		
		String[] columns = {"Parametru", "Valoare"};
		Object[] fields = DataCheck.getMeasurementsFields();
		
		Object[][] data = new Object[fields.length][columns.length];
		
		for (int i = 0; i < fields.length; i++) {
			data[i][0] = fields[i];
			data[i][1] = "";
		}

		return new JTable(data, columns);
	}
	
	public void setCurrentSensorTitleLabel(String title) {
		currentSensorTitle.setText(title);
	}
	
	public void setCurrentSensorAreaLabel(String title) {
		currentSensorArea.setText(title);
	}
	
	public SensorPanel() {
		
		this.setLayout(new BorderLayout());
		this.setBackground(Colors.BRIGHT_YELLOW);	
		this.setPreferredSize(new Dimension(400, 300));
		
		/* JPanel with sensor data (inside of info panel) */
		JPanel sensorDataPanel = new JPanel();
		sensorDataPanel.setLayout(new BorderLayout());
		sensorDataPanel.setBackground(Colors.BRIGHT_YELLOW);
		
		currentSensorTitle = new JLabel();
		currentSensorTitle.setFont(new Font("Arial", Font.BOLD, 18));
		currentSensorTitle.setText("DETALII DESPRE SENZORI AICI");
		
		currentSensorArea = new JLabel();
		currentSensorArea.setFont(new Font("Arial", Font.PLAIN, 16));
		currentSensorArea.setText("DETALII DESPRE ZONA AICI");
		
		latestSensorMeasurement = new JLabel();
		latestSensorMeasurement.setFont(new Font("Arial", Font.BOLD, 14));
		latestSensorMeasurement.setText("Ultima masuratoare: --/--/--");
		
		sensorDataPanel.add(currentSensorTitle, BorderLayout.NORTH);
		sensorDataPanel.add(currentSensorArea, BorderLayout.CENTER);
		sensorDataPanel.add(latestSensorMeasurement, BorderLayout.SOUTH);
		
		/* ADD SENSOR DATA TO SENSOR PANEL */
		this.add(sensorDataPanel, BorderLayout.NORTH);
		
		/* JPanel with JTable of latest sensor measurements */
		JPanel tablePanel = new JPanel(new BorderLayout());
		
		JTable measurementsTable = this.createMeasurementsTable();
		measurementsTable.setFillsViewportHeight(true);
		measurementsTable.setBackground(Colors.BRIGHT_YELLOW);
		
		tablePanel.add(measurementsTable.getTableHeader(), BorderLayout.PAGE_START);
		tablePanel.add(measurementsTable, BorderLayout.CENTER);
		tablePanel.setBackground(Colors.BRIGHT_YELLOW);
	
		/* ADD MEASUREMENTS TABLE TO SENSOR PANEL */
		this.add(tablePanel, BorderLayout.CENTER);
		
		/* JPanel with plot button */
		JPanel sensorActionsPanel = new JPanel();
		sensorActionsPanel.setLayout(new GridBagLayout());
		sensorActionsPanel.setBackground(Colors.BRIGHT_YELLOW);
		
		JButton plotSensorDataBtn = new JButton("Istoric grafic");
		sensorActionsPanel.add(plotSensorDataBtn);
		
		/* ADD SENSOR ACTIONS TO SENSOR PANEL */
		this.add(sensorActionsPanel, BorderLayout.SOUTH);
		
		this.setBorder(new EtchedBorder());
	}
}
