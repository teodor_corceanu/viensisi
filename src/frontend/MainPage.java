package frontend;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.esri.core.geometry.Point;
import com.esri.map.GraphicsLayer;
import com.esri.map.JMap;

import backend.DataCheck;
import backend.Sensor;
import backend.SensorLocator;
import backend.Volunteer;
import main.Project;
import map.RiverMap;

public class MainPage extends JPanel {
	
	private String level;
	private Volunteer user;
	
	/* To suppress warnings */
	private static final long serialVersionUID = 1L;
	
	/* Map dimensions */
	private int mapX = 600;
	private int mapY = 600;
	
	/*
	 * The Action panel stays in the EAST part of the main page 
	 * 	** It displays sensor data whether a sensor is clicked
	 * 	** It displays the black and white logo if the click has no action
	 *  ** It offers the possibility to report an event if a volunteer wishes to
	 *  		(with a right click on a map location)
	 * 
	 */
	private static JPanel actionPanel;
	
	/* 
	 * Panel which contains data of a current clicked sensor
	 * Will occupy the action Panel on some occasions
	 */
	private static SensorPanel sensorPanel;
	
	/*
	 * This allows the user to enter data about an event
	 * Will occupy the action Panel on some occasions
	 */
	private static EventPanel eventPanel;
	
	/* 
	 * Label which contains the black and white logo
	 * Will occupy the action Panel on some occasions
	 */
	private static JLabel logoLabel;
	
	/*
	 * This will be enabled when it is time for a 
	 * volunteer to report an event
	 */
	private JButton reportEventBtn;
	
	private static JPanel mapPanel;
	private static JMap map;
	

	private void showSensorPanel(Sensor s) {
		
		/* Set sensor details */
		sensorPanel.setCurrentSensorTitleLabel(s.getName());
		sensorPanel.setCurrentSensorAreaLabel("Zona " + s.getArea());
		
		/* Set sensor panel visible and the others invisible */
		sensorPanel.setVisible(true);	
		logoLabel.setVisible(false);
		eventPanel.setVisible(false);
		
		/* Add sensor panel to action panel */
		actionPanel.removeAll();
		actionPanel.add(sensorPanel);
	}
	
	public static void showLogoLabel() {
		
		/* Set logo label visible and others invisible */
		logoLabel.setVisible(true);
		sensorPanel.setVisible(false);
		eventPanel.setVisible(false);
		
		/* Add logo label to action panel */
		actionPanel.removeAll();
		actionPanel.add(logoLabel);
	}
	
	private void showEventPanel() {
		
		/* Set event panel visible and others invisible */
		eventPanel.setVisible(true);
		sensorPanel.setVisible(false);
		logoLabel.setVisible(false);
		
		/* Add event panel to action panel */
		actionPanel.removeAll();
		actionPanel.add(eventPanel);
		
	}
	
	public MainPage(String level, Volunteer user) {
		
		if (level != "visitor") {
			this.user = user;
		}
		else {
			this.user = null;
		}
		this.setLayout(new BorderLayout());
		this.setBackground(Colors.BRIGHT_YELLOW);
		
		/* Panel with header (buttons and user label) */
		HeaderPanel headerPanel;
		if (level == "volunteer") {
			headerPanel = new HeaderPanel(user.getUsername());
		}
		else {
			headerPanel = new HeaderPanel("visitor");
		}

		/* Add menu panel to mainpage */
		this.add(headerPanel, BorderLayout.NORTH);
		
		/* Action panel which will alternatively contain Sensor data OR logo OR Event completion data */
		actionPanel = new JPanel();
		actionPanel.setBackground(Colors.BRIGHT_YELLOW);
		actionPanel.setLayout(new BorderLayout());
		actionPanel.setBorder(new EmptyBorder(175, 125, 175, 125));
		this.add(actionPanel, BorderLayout.CENTER);

		/* Panel with current clicked sensor data */
		sensorPanel = new SensorPanel();
		
		/* Logo label to add if the user does not click on a sensor */
		logoLabel = new JLabel();
		logoLabel.setIcon(new ImageIcon("logo-black.png"));
		
		/* Event panel */
		eventPanel = new EventPanel(user);
		
		/* Initially, show logo label */
		this.showLogoLabel();
		
		/* JPanel with map */
		mapPanel = new JPanel();
		mapPanel.setLayout(new GridLayout(1, 1));
		mapPanel.setPreferredSize(new Dimension(mapX, mapY));

		map = RiverMap.getInstance().getMap();
		map.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				/* 
				 * Try to display sensor data 
				 * If user did not click on a sensor, the logo stays
				 */
				Point rawPoint = map.toMapPoint(e.getX(), e.getY());
				Point location = DataCheck.getGPSCoordinates(map, rawPoint);
				SensorLocator sl = new SensorLocator(location);
				
				if (sl.locate()) {
					/* If sensor was located successfully, add its data to actionPanel */
					MainPage.this.showSensorPanel(sl.getLocatedSensor());
				}
				else {
					if (SwingUtilities.isLeftMouseButton(e)) {
						MainPage.this.showLogoLabel();
					}
					else {
						if (!(level.equals("visitor"))) {
							if (e.getClickCount() == 2) {
								/* Allow volunteer to report an event */
								eventPanel.setOccurenceLocation(location);
								MainPage.this.showEventPanel();							
							}							
						}
					}
				}
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				
			}
			
		});
		mapPanel.add(map);
		this.add(mapPanel, BorderLayout.WEST);	
	}
	
	public MainPage(String level) {
		this("visitor", null);
	}
}