package frontend;

import java.awt.BorderLayout;
import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

import backend.AccessDB;

public class PlottingPage extends JPanel {
//+
	private JFrame window;
	private int sizeX = 1200;
	private int sizeY = 700;
//-	
	JPanel selAreaPanel, selParamPanel, plotPagPanel;
	JLabel areaLabel, paramLabel;
	private int areaNo = 1, paramNo = 1;
	private TimeSeries dataSerie;
	private TimeSeriesCollection dataSet;
    private JFreeChart chart;
    private ChartPanel chartPanel;
    private CheckboxGroup groupAreas, groupParams;
    ArrayList<ArrayList<String>> areaSensors, areaValues;
    ArrayList<String> paramNames;
    double minY, maxY;
    
	ArrayList<ChartPanel> cp = new ArrayList<ChartPanel>();
    
	public PlottingPage() {
//+
//		window = new JFrame();
//		window.setPreferredSize(new Dimension(sizeX, sizeY));
//		window.pack();
//		RefineryUtilities.centerFrameOnScreen(window);
//		window.setVisible(true);
//		plotPagPanel => this
		
		/* Title panel */
		JPanel titlePanel = new JPanel();
		titlePanel.setLayout(new GridBagLayout());
		titlePanel.setBackground(Colors.DARK_BLUE);
		
		JLabel title = new JLabel("GRAFICE");
		title.setForeground(Color.WHITE);
		title.setFont(new Font("Verdana", Font.PLAIN, 30));
		
		titlePanel.add(title);
		
//-
		this.setBackground(Colors.BRIGHT_YELLOW);
		this.setLayout(new BorderLayout());
		plotPagPanel = new JPanel();
		plotPagPanel.setLayout(new GridBagLayout());
		plotPagPanel.setBackground(Colors.BRIGHT_YELLOW);
		areaValues = new ArrayList<ArrayList<String>>();
/*		
		headerPanel = new HeaderPanel(this.username);
		this.add(headerPanel, BorderLayout.NORTH);
*/		
		selAreaPanel = new JPanel();
		selAreaPanel.setLayout(new GridLayout(0,1));
		selAreaPanel.setBackground(Colors.BRIGHT_YELLOW);
		selAreaPanel.setPreferredSize(new Dimension(200, 500));
		areaLabel = new JLabel("Selectati Zona:");
		selAreaPanel.add(areaLabel);
		groupAreas =  new CheckboxGroup();
		createAreasList(selAreaPanel, groupAreas);
		selParamPanel = new JPanel();
		selParamPanel.setLayout(new GridLayout(0,1));
		selParamPanel.setBackground(Colors.BRIGHT_YELLOW);
		selParamPanel.setPreferredSize(new Dimension(200, 500));
		paramLabel = new JLabel("Selectati Parametrul:");
		selParamPanel.add(paramLabel);
		groupParams =  new CheckboxGroup();
		createParamList(selParamPanel, groupParams);

		dataSet = createDataSet();
		chart = createChart(paramNames.get(paramNo + 2), dataSet);
		chartPanel = new ChartPanel(chart);
		chartPanel.setBackground(Colors.BRIGHT_YELLOW);
		plotPagPanel.add(selAreaPanel);
		plotPagPanel.add(selParamPanel);
		plotPagPanel.add(chartPanel);
		
		add(titlePanel, BorderLayout.NORTH);
		add(plotPagPanel, BorderLayout.CENTER);
		
		/* Back btn */
		JPanel backBtnPanel = new JPanel();
		backBtnPanel.setLayout(new GridBagLayout());
		backBtnPanel.setBackground(Colors.BRIGHT_YELLOW);
		
		JButton backBtn = new JButton("Inapoi");
		backBtn.addActionListener(new ActionListener() {
		
			@Override
			public void actionPerformed(ActionEvent e) {
			
			}
			
		});
		
		backBtnPanel.add(backBtn);
		backBtnPanel.setBorder(new EmptyBorder(30, 30, 30, 30));
		
		add(backBtnPanel, BorderLayout.SOUTH);
		
	}
	
	private void changeSerie() {

		
		chartPanel.setVisible(false);
		plotPagPanel.remove(chartPanel);
		
		dataSet = createDataSet();
		chart = createChart(paramNames.get(paramNo + 2), dataSet);
		chartPanel = new ChartPanel(chart);	
		//chartPanel.setPreferredSize(new Dimension(700, 500));
		chartPanel.repaint();
		//plotPagPanel.add(chartPanel);
		chartPanel.setVisible(true);
		plotPagPanel.add(chartPanel);
 
	//+	
		//this.add(plotPagPanel);

	//-    
	}
	
	private TimeSeriesCollection createDataSet() {
		int j = paramNo + 2;
		int intValue;
		double doubValue;
		RegularTimePeriod regDateTime;
		Date dateTime;
		dataSerie = new TimeSeries("Random Data", Millisecond.class);
		minY = maxY = -1;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateTime = new Date();
//		System.out.println("Size="+areaValues.size()+" ParamNo="+ paramNo);
		for(int i = 1; i < areaValues.size(); i++) {		
			try {
				dateTime = format.parse(areaValues.get(i).get(2));
			} 
			catch (ParseException e) {
				e.printStackTrace();
			}
			regDateTime = new Millisecond (dateTime);
//			System.out.println(i+"||"+areaValues.get(i).get(j));
			if(j == 4) {
				doubValue = Double.parseDouble(areaValues.get(i).get(j));
//				System.out.println(regDateTime + "|real|" + doubValue);
				dataSerie.addOrUpdate(regDateTime, doubValue);
				if((minY == -1) || (doubValue < minY)) {
					minY = doubValue;
				}
				if((maxY == -1) || (doubValue > maxY)) {
					maxY = doubValue;
				}
			}
			else {
				intValue = Integer.parseInt(areaValues.get(i).get(j));
//				System.out.println(regDateTime + "|int|" + intValue);
				dataSerie.addOrUpdate(regDateTime, intValue);
				if((minY == -1) || (intValue < minY)) {
					minY = intValue;
				}
				if((maxY == -1) || (intValue > maxY)) {
					maxY = intValue;
				}
			}
		}
	   return new TimeSeriesCollection(dataSerie);
	 }
	
	private JFreeChart createChart(String title, final XYDataset dataset) {
        final JFreeChart result = ChartFactory.createTimeSeriesChart(title, "Time", "Value", dataset, true, true, false);
        final XYPlot plot = result.getXYPlot();
        ValueAxis axis = plot.getDomainAxis();
        axis.setAutoRange(true);
        axis.setFixedAutoRange(864000000.0);  // milliseconds = 10 zile Axa OX
        axis = plot.getRangeAxis();
        axis.setRange(minY, maxY); 			  // Axa OY
        return result;
    }
	
	private void getAllValues() {
		String s = groupAreas.getSelectedCheckbox().getLabel();
		areaNo = Integer.valueOf(s.substring(0, s.indexOf('.')));
		
		try {
			areaSensors = AccessDB.select("viensisi.sensors", "sensor_id", "area_id = " + areaNo);
			String cond = "(sensor_id = " + areaSensors.get(1).get(0) + ")";
			for(int j = 2; j < areaSensors.size(); j++) {
				cond = cond + " OR (sensor_id = " + areaSensors.get(j).get(0) + ")";
			}
			cond = cond + " ORDER BY date_time";
			try {
				areaValues = AccessDB.select("viensisi.measurements", "*", cond);
				if(dataSet != null) {
					changeSerie();
					dataSet.seriesChanged(null);
				}
			}
			catch (Exception em) {
				em.printStackTrace();
			}
		}
		catch (Exception es) {
			es.printStackTrace();
		}
		
	}
	
	 private void createAreasList(JPanel panel, CheckboxGroup group) {
		 try {
			ArrayList<ArrayList<String>> result = AccessDB.select("viensisi.areas", "*", "");
			for (int i = 1; i < result.size(); i++) {
				Checkbox ck = new Checkbox(result.get(i).get(0).toString()+". "+ result.get(i).get(1), group, false);
				if(i == areaNo) {
					groupAreas.setSelectedCheckbox(ck);
					getAllValues();
				}
				final Integer innerI = new Integer(i);
				ck.addItemListener(new ItemListener() {
					@Override
					public void itemStateChanged(ItemEvent e) {
						areaNo = innerI;
						getAllValues();
					}
				});
				panel.add(ck);
			}
			if(dataSet != null) {
				changeSerie();
				dataSet.seriesChanged(null);
			}
		}
		catch (Exception ea) {
			ea.printStackTrace();
		}
	}
	 
	 private void createParamList(JPanel panel, CheckboxGroup group) {
		try {
			ArrayList<ArrayList<String>> result = AccessDB.select("viensisi.measurements", "*", "");
			String parName =" ", field;
			paramNames = new ArrayList<String>();
			paramNames.add("measurement_id");
			paramNames.add("sensor_id");
			paramNames.add("date_time");
			for (int j = 3; j < result.get(0).size(); j++) {
				field = result.get(0).get(j);
				if (field.equals("opacity")) {
					parName = (j - 2) + ". Turbiditate";
					paramNames.add("Turbiditate");
				}
				if (field.equals("ph")) {
					parName = (j - 2) + ". PH";
					paramNames.add("PH");
				}
				if (field.equals("temperature")) {
					parName = (j - 2) + ". Temperatura";
					paramNames.add("Temperatura");
				}
				if (field.equals("fixed_residue")) {
					parName = (j - 2) + ". Reziduu fix";
					paramNames.add("Reziduu fix");
				}
				if (field.equals("dissolved_oxygen")) {
					parName = (j - 2) + ". Oxigen dizolvat";
					paramNames.add("Oxigen dizolvat");
				}
				if (field.equals("cyanides")) {
					parName = (j - 2) + ". Concentratie cianuri";
					paramNames.add("Concentratie cianuri");
				}
				if (field.equals("chlorides")) {
					parName = (j - 2) + ". Concentratie cloruri";
					paramNames.add("Concentratie cloruri");
				}
				Checkbox ck = new Checkbox(parName, group, false);
				if(j == paramNo + 2) {
					group.setSelectedCheckbox(ck);
					
				}
				final Integer innerJ = new Integer(j);
				ck.addItemListener(new ItemListener() {
					@Override
					public void itemStateChanged(ItemEvent e) {							
						String s = groupParams.getSelectedCheckbox().getLabel();
						paramNo = Integer.valueOf(s.substring(0, s.indexOf('.')));
						paramNo = innerJ - 2;
						if(dataSet != null) {
							changeSerie();
							dataSet.seriesChanged(null);
						}
					}
				});
				panel.add(ck);
			}
			if(dataSet != null) {
				dataSet.seriesChanged(null);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
}
