package frontend;

import java.awt.GridBagLayout;

import javax.swing.JPanel;

import backend.Volunteer;

public class SettingsPage extends JPanel {

	private Volunteer user;
	
	public SettingsPage(Volunteer user) {
		
		this.user = user;
		this.setLayout(new GridBagLayout());
		this.setBackground(Colors.BRIGHT_YELLOW);
	}
	
	public Volunteer getUser() {
		return user;
	}
}
