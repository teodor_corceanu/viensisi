package frontend;

import java.awt.Color;

public final class Colors {

	private Colors() {}
	
	public static final Color BRIGHT_YELLOW = new Color(254, 253, 166);
	public static final Color DARK_BLUE = new Color(0, 44, 89);
	public static final Color BRIGHT_GREEN = new Color(193, 241, 196);

}
